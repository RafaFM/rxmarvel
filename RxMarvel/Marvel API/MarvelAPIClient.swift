//
//  MarvelAPIClient.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireImage

class MarvelAPIClient: APIClientProtocol {
    // MARK: - Typealias
    typealias Resource =  MarvelAPIResourceProtocol
    typealias ResponseCompletionHandler = Result<Decodable, MarvelAPIError>
    
    // MARK: - Properties
    // MARK: Static
    static let shared = MarvelAPIClient(apiURL: MarvelBaseURL(), authenticationData: MarvelAPIAuthenticationData())
    
    // MARK: Public
    var authenticationObject: MarvelAPIAuthenticationData?
    var baseURL: URL = MarvelBaseURL().url
    
    // MARK: Private
    private let alamofireManager: SessionManager
    
    // MARK: - Initializers
    private init(apiURL: MarvelBaseURL, authenticationData: MarvelAPIAuthenticationData) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
        
        authenticationObject = authenticationData
    }
    
    // MARK: - Image Methods
    public func fetchImage(fromUrl url: URL, imageView: UIImageView, placeholder: UIImage?, completion: ((Bool)->())?) {
        imageView.af_setImage(withURL: url,
                              placeholderImage: placeholder,
                              imageTransition: .crossDissolve(0.2)) { (response) in
                                completion?(true)
                                imageView.image = response.result.value
        }
    }
}

// MARK: - APIClientProtocol
extension MarvelAPIClient {
    func performCall<T>(with resource: MarvelAPIResourceProtocol, response: T.Type, completion: @escaping (Result<Decodable, MarvelAPIError>) -> ()) where T : Decodable {
        switch resource.httpMethod {
        case .get:
            getRequest(with: resource, response: response, completion: completion)
        case .post:
            print("post")
        }
    }
    
    func authenticate(_ url: inout URL, with authenticationObject: MarvelAPIAuthenticationData?) {
        guard let authenticationParameters = authenticationObject?.authenticationInfo.authenticationParameters else { return }
        
        var components = URLComponents(url: url,
                                       resolvingAgainstBaseURL: false)
        
        components?.queryItems = authenticationParameters.map ( { URLQueryItem(name: String($0), value: String(describing: $1)) } )
        
        url = components!.url!
    }
}

extension MarvelAPIClient {
    
    private func getEndPoint(to resource: MarvelAPIResourceProtocol) -> URL? {
        return baseURL.appendingPathComponent(resource.path)
    }
    
    private func getFinalUrl(to resource: MarvelAPIResourceProtocol) -> URL {
        var url = getEndPoint(to: resource)!
        authenticate(&url, with: authenticationObject)
        return url
    }
    
    private func getRequest<T: Decodable>(with resource: MarvelAPIResourceProtocol, response: T.Type, completion: @escaping (ResponseCompletionHandler) -> ()) {
        let finalUrl = getFinalUrl(to: resource)
        
        alamofireManager.request(finalUrl,
                                 parameters: resource.parameters)
            .validate()
            .responseJSON { (response) in
                guard response.result.isSuccess else {
                    completion(Result.failure(.responseUnsuccessful))
                    return
                }
                
                guard let jsonData = response.data,
                    let results = try? JSONDecoder().decode(T.self, from: jsonData) else {
                        completion(Result.failure(.jsonConversionFailure))
                        return
                }
                completion(Result.success(results))
        }
    }
}
