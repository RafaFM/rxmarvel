//
//  ThumbnailModel.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

enum ThumbnailSize: String {
    case small = "standard_small"
    case medium = "standard_medium"
    case large = "standard_large"
    case xlarge = "standard_xlarge"
    case fantastic = "standard_fantastic"
    case amazing = "standard_amazing"
}

struct Thumbnail: Decodable {
    let url: URL
    
    init(thumbnailUrl: URL) {
        url = thumbnailUrl
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let path = try container.decode(String.self, forKey: .path)
        let ext = try container.decode(String.self, forKey: .ext)
        let pathHttps = path.replacingOccurrences(of: "http:", with: "https:", options: .literal, range: nil)
        let thumbnailUrl = URL(string: "\(pathHttps)/\(ThumbnailSize.fantastic.rawValue).\(ext)")!

        self.init(thumbnailUrl: thumbnailUrl)
    }

    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}
