//
//  ComicModel.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

struct ComicDataModel: MarvelDataProtocol {
    var data: ComicListModel
}

struct ComicListModel: MarvelAPIListProtocol {
    var offset: Int
    var limit: Int
    var total: Int
    var count: Int
    var results: [ComicModel]
}

struct ComicModel: Decodable {
    let id: Int
    let title: String
    let description: String?
    let pageCount: Int
    let isbn: String
    let modified: String
    let thumbnail: Thumbnail
}
