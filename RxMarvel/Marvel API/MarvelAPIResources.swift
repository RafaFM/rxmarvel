//
//  MarvelAPIRequests.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

enum Method {
    case get
    case post
}

protocol MarvelAPIResourceProtocol {
    var path: String { get }
    var httpMethod: Method { get }
    var parameters: [String: Any]? { get }
}

extension MarvelAPIResourceProtocol {
    var httpMethod: Method { return .get }
    
    var parameters: [String: Any]? {
        return [:]
    }
}

struct MarvelAPIResources {
    // Extend this struct for request
    // The new nested structs has to conform APIResourceProtocol
}
