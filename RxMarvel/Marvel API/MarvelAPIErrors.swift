//
//  MarvelAPIErrors.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

enum MarvelAPIError: Error {
    case responseUnsuccessful
    case jsonConversionFailure
    
    var localizedDescription: String {
        switch self {
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        }
    }
}
