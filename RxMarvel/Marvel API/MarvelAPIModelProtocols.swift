//
//  MarvelAPIModelProtocols.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

protocol MarvelDataProtocol: Decodable {
    associatedtype T
    var data: T { get set }
}

protocol MarvelAPIListProtocol: Decodable {
    associatedtype T
    
    var offset: Int { get set }
    var limit: Int { get set }
    var total: Int { get set }
    var count: Int { get set }
    var results: [T] { get set }
}
