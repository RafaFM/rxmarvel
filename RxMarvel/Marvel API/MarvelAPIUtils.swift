//
//  MarvelAPIUtils.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

struct MarvelBaseURL {
    private let apiVersion: String = "1"
    
    private var baseUrl: String {
        get {
            return "https://gateway.marvel.com/v%@/public"
        }
    }
    
    var url: URL {
        get {
            let finalUrl = baseUrl.replacingOccurrences(of: "%@", with: apiVersion, options: .literal, range: nil)
            return URL(string: finalUrl)!
        }
    }
}
