//
//  MarvelAPIAuthenticationData.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

import CryptoSwift

struct MarvelAPIAuthenticationData: AuthenticationDataProtocol {
    typealias T = Credentials
    
    struct Credentials {
        private let privateKey = "aeb78829714ebd706314728dfba1bd42ea3eaf77"
        private let publicKey = "a3543994f37f1b53d28727494ff667c9"
        
        private let timeStamp: String = {
            return NSDate().timeIntervalSince1970.description
        }()
        
        var authenticationParameters: [String: Any] {
            let hashable = "\(timeStamp)\(privateKey)\(publicKey)"
            return [
                "ts": timeStamp,
                "apikey": publicKey,
                "hash": hashable.md5()
            ]
        }
    }
    
    var authenticationInfo = Credentials()
}
