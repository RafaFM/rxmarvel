//
//  ComicListViewModel.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class ComicListViewModel {
    // MARK: - Properties
    // MARK: Private
    private let disposeBag = DisposeBag()
    private let _comics = BehaviorRelay<[ComicModel]>(value: [])
    private let _isFetching = BehaviorRelay<Bool>(value: false)
    private let _error = BehaviorRelay<String?>(value: nil)
    
    // MARK: Public
    var isFetching: Driver<Bool> {
        return _isFetching.asDriver()
    }
    var comics: Driver<[ComicModel]> {
        return _comics.asDriver()
    }
    var error: Driver<String?> {
        return _error.asDriver()
    }
    var hasError: Bool {
        return _error.value != nil
    }
    var numberOfComics: Int {
        return _comics.value.count
    }
    
    // MARK: - Initializer
    init(textToSearch text: Driver<String>) {
        text
            .throttle(1.0)
            .distinctUntilChanged()
            .drive(onNext: { [weak self] (textToSearch) in
                let resource = MarvelAPIResources.ComicsList(textToSearch: textToSearch)
                self?.fetchComics(fromResource: resource)
                if textToSearch.isEmpty {
                    self?._comics.accept([])
                }
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Utils
    func viewModelForComic(at index: Int) -> ComicViewModel? {
        guard index < _comics.value.count else {
            return nil
        }
        return ComicViewModel(comic: _comics.value[index])
    }
    
    func detailViewModelForComic(at index: Int) -> ComicDetailViewModel? {
        guard index < _comics.value.count else {
            return nil
        }
        return ComicDetailViewModel(comic: _comics.value[index])
    }
    
    // MARK: - Fetch
    func fetchComics(fromResource resource: MarvelAPIResources.ComicsList) {
        self._comics.accept([])
        self._isFetching.accept(true)
        self._error.accept(nil)
        
        MarvelAPIClient.shared.performCall(with: resource,
                                           response: ComicDataModel.self) {[weak self] (result) in
                                            switch result {
                                            case .success(let success):
                                                let comicDataModel = success as! ComicDataModel
                                                self?._isFetching.accept(false)
                                                self?._comics.accept(comicDataModel.data.results)
                                            case .failure( let error):
                                                self?._isFetching.accept(false)
                                                self?._error.accept(error.localizedDescription)
                                            }
        }
    }
}
