//
//  ComicViewModel.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import UIKit

struct ComicViewModel {
    private var comic: ComicModel
    
    init(comic: ComicModel) {
        self.comic = comic
    }
    
    var title: String {
        return comic.title
    }
    
    var isbn: String {
        if comic.isbn.isEmpty {
            return "ISBN: -"
        } else {
            return "ISBN: \(comic.isbn)"
        }
    }
    
    var modifiedDate: String {
        let date = DateUtils.stringToDate(comic.modified)
        return DateUtils.dateToString(date, withDateStyle: .medium)
    }
    
    var coverImageUrl: URL {
        return comic.thumbnail.url
    }
    
    func fetchCover(imageView: UIImageView, placeholder: UIImage?, completion: @escaping (Bool)->()) {
        MarvelAPIClient.shared.fetchImage(fromUrl: coverImageUrl,
                                          imageView: imageView,
                                          placeholder: placeholder,
                                          completion: completion)
    }
}
