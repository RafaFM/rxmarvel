//
//  ComicListTableViewCell.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import UIKit

class ComicListTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var isbnLabel: UILabel!
    @IBOutlet weak var modifiedDateLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var imageActivityIndicatorView: UIActivityIndicatorView!
    
    static let identifier = String(describing: ComicListTableViewCell.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        coverImageView.image = UIImage(named: "placeholder")
    }

    func configure(viewModel: ComicViewModel) {
        titleLabel.text = viewModel.title
        isbnLabel.text = viewModel.isbn
        modifiedDateLabel.text = viewModel.modifiedDate
        
        imageActivityIndicatorView.startAnimating()
        
        viewModel.fetchCover(imageView: self.coverImageView,
                             placeholder: UIImage(named: "placeholder")) { (finished) in
                                self.imageActivityIndicatorView.stopAnimating()
        }
    }
}
