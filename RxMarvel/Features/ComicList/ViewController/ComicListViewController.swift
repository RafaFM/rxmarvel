//
//  ComicListViewController.swift
//  RxMarvel
//
//  Created by Rafa on 13/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

class ComicListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Properties
    fileprivate var comicListViewModel: ComicListViewModel!
    fileprivate let disposeBag = DisposeBag()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViews()
        configureUI()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let comicDetailVC = segue.destination as? ComicDetailViewController,
            let index = tableView.indexPathForSelectedRow?.row,
            let viewModel = comicListViewModel.detailViewModelForComic(at: index) {
            comicDetailVC.viewModel = viewModel
        }
    }
}

// MARK: - UI Configuration
extension ComicListViewController {
    fileprivate func bindViews() {
        comicListViewModel = ComicListViewModel(textToSearch: searchBar.rx.text.orEmpty.asDriver())
        
        comicListViewModel.comics.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: ComicListTableViewCell.identifier, cellType: ComicListTableViewCell.self)) {
                row, comic, cell in
                if let viewModel = self.comicListViewModel.viewModelForComic(at: row) {
                    cell.configure(viewModel: viewModel)
                }
            }
            .disposed(by: disposeBag)
        
        comicListViewModel.isFetching
            .drive(activityIndicatorView.rx.isAnimating)
            .disposed(by: disposeBag)
    }
    
    fileprivate func configureUI() {
        searchBar.placeholder = "Search your comics"
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
    }
}

