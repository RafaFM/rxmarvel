//
//  ComicsListResources.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

extension MarvelAPIResources {
    struct ComicsList: MarvelAPIResourceProtocol {
        
        var textToSearch: String
        var path: String { return "/comics" }

        
        var parameters: [String: Any]? {
            
            var params: [String: Any]?
            
            if !textToSearch.isEmpty {
                params = [
                    "titleStartsWith": textToSearch
                ]
            }
            
            return params
        }
    }
}
