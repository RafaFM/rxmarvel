//
//  ComicDetailViewController.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import UIKit

class ComicDetailViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    // MARK: - Properties
    // MARK: Public
    public var viewModel: ComicDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
}

// MARK: - UI Configuration
extension ComicDetailViewController {
    fileprivate func configureUI() {
        title = viewModel.title
        descriptionTextView.text = viewModel.description
        viewModel.fetchCover(imageView: coverImageView,
                             placeholder: UIImage(named: "placeholder"))
    }
}
