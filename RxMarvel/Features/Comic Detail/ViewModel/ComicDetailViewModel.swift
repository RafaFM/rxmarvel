//
//  ComicDetailViewModel.swift
//  RxMarvel
//
//  Created by Rafa on 15/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import UIKit

struct ComicDetailViewModel {
    private var comic: ComicModel
    
    init(comic: ComicModel) {
        self.comic = comic
    }
    
    var title: String {
        return comic.title
    }
    
    var description: String {
        guard let description = comic.description else {
            return "No description available"
        }

        return description
    }
    
    var pageCount: String {
        return comic.pageCount.description
    }
    
    var coverImageUrl: URL {
        return comic.thumbnail.url
    }
    
    func fetchCover(imageView: UIImageView, placeholder: UIImage?) {
        MarvelAPIClient.shared.fetchImage(fromUrl: coverImageUrl,
                                          imageView: imageView,
                                          placeholder: placeholder,
                                          completion: nil)
    }
}
