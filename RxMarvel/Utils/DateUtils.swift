//
//  DateUtils.swift
//  RxMarvel
//
//  Created by Rafa on 14/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

class DateUtils {
    private static var UTCDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        return formatter
    }()

    static func stringToDate(_ strDate: String) -> Date? {
        return UTCDateFormatter.date(from: strDate)
    }
   
    static func dateToString(_ date: Date?, withDateStyle style: DateFormatter.Style? = nil) -> String {
        guard let theDate = date else {
            return ""
        }
        
        guard let style = style else {
            return UTCDateFormatter.string(from: theDate)
        }
        
        let formatter = DateFormatter()
        formatter.dateStyle = style
        return formatter.string(from: theDate)
    }
}
