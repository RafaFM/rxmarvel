//
//  APIClient.swift
//  RxMarvel
//
//  Created by Rafa on 13/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

protocol APIClientProtocol {
    associatedtype T = AuthenticationDataProtocol
    associatedtype ResponseCompletionHandler
    associatedtype Resource

    var authenticationObject: T? { get set }
    var baseURL: URL { get set }

    func performCall<T: Decodable>(with resource: Resource, response: T.Type, completion: @escaping (ResponseCompletionHandler) -> () )
    func authenticate(_ url: inout URL, with authenticationObject: T?)
}
