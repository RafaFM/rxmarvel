//
//  APIAuthenticationDataProtocol.swift
//  RxMarvel
//
//  Created by Rafa on 13/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

protocol AuthenticationDataProtocol {
    associatedtype T
    var authenticationInfo: T { get set }
}
