//
//  Result.swift
//  RxMarvel
//
//  Created by Rafa on 13/04/2019.
//  Copyright © 2019 Rafa Ferrero. All rights reserved.
//

import Foundation

enum Result<T, U: Error> {
    case success(T)
    case failure(U)
}

